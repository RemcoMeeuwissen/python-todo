import pytest
import sqlite3
from todo import Items, Database

conn = sqlite3.connect(":memory:")
database = Database(conn)
database.create()
items = Items(conn)

def test_all_returns_all_items():
    items.add('Test')
    items.add('Test 2')

    result = items.all()

    assert len(result) == 2
    assert result[0] == (1, 'Test', 0)
    assert result[1] == (2, 'Test 2', 0)
