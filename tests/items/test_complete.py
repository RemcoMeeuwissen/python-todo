import pytest
import sqlite3
from mock import patch
from todo import Items, Database

conn = sqlite3.connect(":memory:")
database = Database(conn)
database.create()
items = Items(conn)

def test_complete_raises_error_for_invalid_input():
    with pytest.raises(ValueError, message='Invalid input'):
        items.complete('')

def test_complete_raises_error_for_invalid_id():
    with pytest.raises(ValueError, message='Invalid id'):
        items.complete(100)

def test_complete_raises_error_when_unable_to_remove_a_item():
    with patch('test_complete.sqlite3') as mocksql:
        mocksql.connect().cursor().fetchall = lambda: (1, )
        mocksql.connect().cursor().rowcount = 0
        items = Items(mocksql.connect())

        with pytest.raises(RuntimeError, message='Unable to complete item'):
            items.complete(1)

def test_complete_can_toggle_the_complete_state_of_a_item():
    items.add('Test')

    c = conn.cursor()

    c.execute("SELECT * FROM items WHERE id=1")
    row = c.fetchone()
    assert row[2] == 0

    items.complete(1)

    c.execute("SELECT * FROM items WHERE id=1")
    row = c.fetchone()
    assert row[2] == 1

    # When we call the complete function again it should mark the item as uncompleted
    items.complete(1)

    c.execute("SELECT * FROM items WHERE id=1")
    row = c.fetchone()
    assert row[2] == 0
