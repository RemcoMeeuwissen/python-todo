import pytest
import sqlite3
from mock import patch
from todo import Items, Database

conn = sqlite3.connect(":memory:")
database = Database(conn)
database.create()
items = Items(conn)

def test_add_raises_error_for_invalid_input():
    with pytest.raises(ValueError, message='Invalid input'):
        items.add('')

def test_add_raises_error_when_unable_to_save_a_item():
    with patch('test_add.sqlite3') as mocksql:
        mocksql.connect().cursor().rowcount = 0
        items = Items(mocksql.connect())

        with pytest.raises(RuntimeError, message='Unable to save item'):
            items.add('Test')

def test_add_can_save_items():
    assert items.add('Test') == 1
    assert items.add('Test 2') == 2

    c = conn.cursor()
    c.execute('SELECT * FROM items')
    assert len(c.fetchall()) == 2
