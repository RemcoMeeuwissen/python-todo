import pytest
import sqlite3
from mock import patch
from todo import Items, Database

conn = sqlite3.connect(":memory:")
database = Database(conn)
database.create()
items = Items(conn)

def test_remove_raises_error_for_invalid_input():
    with pytest.raises(ValueError, message='Invalid input'):
        items.remove('')

def test_remove_raises_error_for_invalid_id():
    with pytest.raises(ValueError, message='Invalid id'):
        items.remove(100)

def test_remove_raises_error_when_unable_to_remove_a_item():
    with patch('test_remove.sqlite3') as mocksql:
        mocksql.connect().cursor().fetchall = lambda: (1, )
        mocksql.connect().cursor().rowcount = 0
        items = Items(mocksql.connect())

        with pytest.raises(RuntimeError, message='Unable to remove item'):
            items.remove(1)

def test_can_remove_items():
    c = conn.cursor()

    items.add('Test')
    items.add('Test 2')

    c.execute('SELECT * FROM items')
    assert len(c.fetchall()) == 2

    assert items.remove(1) == True

    c.execute('SELECT * FROM items')
    assert len(c.fetchall()) == 1
