class Database:
    conn = False

    def __init__(self, conn):
        self.conn = conn

    def create(self):
        c = self.conn.cursor();

        # See if the items table already exists
        c.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='items'")
        rows = len(c.fetchall())

        if rows == 0:
            # Create the items table if it doesn't exist
            c.execute("""
CREATE TABLE `items` (
    `id`        INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    `name`      TEXT NOT NULL,
    `completed` INTEGER NOT NULL
)
            """)
