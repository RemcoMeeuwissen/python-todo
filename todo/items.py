class Items:
    conn = False

    def __init__(self, conn):
        self.conn = conn

    def all(self):
        c = self.conn.cursor()

        c.execute("SELECT * FROM items")
        rows = c.fetchall()

        return rows

    def add(self, item):
        if not item:
            raise ValueError('Invalid input')

        c = self.conn.cursor()

        c.execute("INSERT INTO items (name, completed) VALUES (?, ?)", (item, 0))
        result = c.rowcount

        if result <= 0:
            raise RuntimeError('Unable to save item')
        else:
            self.conn.commit()
            return c.lastrowid

    def complete(self, id):
        if not id:
            raise ValueError('Invalid input')

        c = self.conn.cursor()

        c.execute("SELECT * FROM items WHERE id=?", (id, ))
        row = c.fetchone()

        if not row:
            raise ValueError('Invalid id')

        # not row[2] inverts the current completed state, 1 to 0 and 0 to 1
        c.execute("UPDATE items SET completed=? WHERE id=?", (not row[2], id))
        result = c.rowcount

        if result <= 0:
            raise RuntimeError('Unable to complete item')
        else:
            self.conn.commit()
            return True


    def remove(self, id):
        if not id:
            raise ValueError('Invalid input')

        c = self.conn.cursor()

        c.execute("SELECT * FROM items WHERE id=?", (id, ))
        row = c.fetchall()

        if not row:
            raise ValueError('Invalid id')

        c.execute("DELETE FROM items WHERE id=?", (id, ))
        result = c.rowcount

        if result <= 0:
            raise RuntimeError('Unable to remove item')
        else:
            self.conn.commit()
            return True
