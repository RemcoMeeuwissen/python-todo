import os
import sqlite3
from flask import Flask, render_template, request, flash, redirect
from todo import Items, Database

app = Flask(__name__)
app.secret_key = 'some_secret'

path = os.path.dirname(os.path.abspath(__file__))
conn = sqlite3.connect(path + '/db.sqlite')
database = Database(conn)
database.create()
items = Items(conn)

@app.route('/', methods=['GET'])
def todo():
    items_todo = filter(lambda item: not bool(item[2]), items.all())
    items_done = filter(lambda item: bool(item[2]), items.all())

    return render_template('todo.html', todo=items_todo, done=items_done)

@app.route('/items', methods=['POST'])
def add_item():
    try:
        items.add(request.form['item'])
    except ValueError:
        flash('An item cannot be empty')
    except RuntimeError:
        flash('Could not save your item, please try again later')

    return redirect('/')

@app.route('/items/<id>', methods=['PUT'])
@app.route('/items/<id>/complete', methods=['GET'])
def complete_item(id):
    try:
        items.complete(id)
    except ValueError as e:
        if e == 'Invalid input':
            flash('Please enter a id')
        else:
            flash('We could not find a item with that id')
    except RuntimeError:
        flash('We could not update the item, please try again later')

    return redirect('/')

@app.route('/items/<id>', methods=['DELETE'])
@app.route('/items/<id>/delete', methods=['GET'])
def remove_item(id):
    try:
        items.remove(id)
    except ValueError as e:
        if e == 'Invalid input':
            flash('Please enter a id')
        else:
            flash('We could not find a item with that id')
    except RuntimeError:
        flash('We could not remove the item, please try again later')

    return redirect('/')
